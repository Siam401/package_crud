<?php

use Illuminate\Routing\Route;
use Vendor\siam\CrudController;

/**
 * Unit Routes
 */
Route::get('/units', [CrudController::class, 'index'])->name('unit.index');
Route::any('/unit/search', [CrudController::class, 'search'])->name('unit.search');
Route::get('/unit/form', [CrudController::class, 'addform'])->name('unit.form');
Route::post('/unit/store', [CrudController::class, 'store'])->name('unit.store');
Route::get('/unit/edit/{unit}', [CrudController::class, 'edit'])->name('unit.edit');
Route::put('/unit/edit/{unit}', [CrudController::class, 'update'])->name('unit.update');
Route::delete('/unit/edit/{unit}', [CrudController::class, 'remove']);
Route::get('/units//list', [UnitController::class, 'list'])->name('unit.list');
