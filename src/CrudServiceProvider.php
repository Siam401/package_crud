<?php

namespace Vendor\siam;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('siam\crud\CrudController');
        $this->loadViewsFrom(__DIR__ . '/../views', 'crud');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Artisan::call('make:model Unit');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->publishes([
            __DIR__ . '/../database/migrations' => database_path('migrations'),
        ], 'crud');

        $this->publishes([
            __DIR__ . '/../views/index.php' => resource_path('views');,
        ], 'crud');

        include __DIR__ . "/routes.php";
    }
}