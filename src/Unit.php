<?php

namespace Vendor\siam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'code',
        'head',
        'status',
    ];

    public static function unitHeads($id = null)
    {
        $data = [
            1 => 'Unit Head 1',
            2 => 'Unit Head 2',
            3 => 'Unit Head 3',
            4 => 'Unit Head 4',
            5 => 'Unit Head 5',
        ];

        if (!empty($id) && isset($data[$id])) {
            return $data[$id];
        }

        return $data;
    }

    public static function modifyValue($unit)
    {
        if ($unit instanceof \Illuminate\Support\Collection) {
            foreach ($unit as $val) {
                $val->head = $val->unitHeads($val->head);
                $val->status = $val->status ? 'Yes' : 'No';
            }
        } elseif ($unit instanceof Unit) {
            $unit->head = $unit->unitHeads($unit->head);
            $unit->status = $unit->status ? 'Yes' : 'No';
        }

        return $unit;
    }

    public static function getAll(array $filter = [])
    {
        $units = Unit::select('name', 'head', 'status', 'code', 'id');

        if (isset($filter['term']) && !empty($filter['term'])) {
            $units->where('name', 'like', "%{$filter['term']}%")
                ->orWhere('code', 'like', "%{$filter['term']}%");
        }

        return Unit::modifyValue(
            $units->orderBy('name', 'asc')->get()
        )->toArray();
    }
}
