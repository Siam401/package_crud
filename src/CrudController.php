<?php


namespace Vendor\siam;

use App\Core\BaseController;
use App\Domains\SimplePage\Http\Requests\UnitStoreRequest;
use App\Domains\SimplePage\Http\Requests\UnitUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CrudController extends BaseController
{

    public function list()
    {
        $units = Unit::modifyValue(
            Unit::orderBy('name', 'asc')->get()
        );

        return view('crud::index', [
            'units' => $units,
            'heads' => Unit::unitHeads(),
        ]);
    }

    public function index()
    {
        $units = Unit::select('name', 'head', 'status', 'code', 'id');

        $result = Unit::modifyValue(
            $units->orderBy('name', 'asc')->get()
        );

        $viewHtml = view('simple_page_view::raw.list')->with([
            'units' => $result,
        ])->render();

        return $this->success()->response([
            'html' => $viewHtml
        ]);
    }

    public function addform()
    {
        $viewHtml = view('simple_page_view::raw.add')->with([
            'title' => 'Unit Create Form',
            'heads' => Unit::unitHeads(),
        ])->render();

        return $this->success()->response([
            'html' => $viewHtml
        ]);
    }

    public function search(Request $request)
    {
        $units = Unit::select('name', 'head', 'status', 'code', 'id');


        if ($request->term !== '' && strlen($request->term) > 0) {
            $units = $units->where('name', 'like', "%{$request->term}%")->orWhere('code', 'like', "%{$request->term}%");
        }

        $result = Unit::modifyValue(
            $units->orderBy('name', 'asc')->get()
        );

        $viewHtml = view('simple_page_view::raw.list')->with([
            'units' => $result,
            'term' => $request->term,
        ])->render();

        return $this->success()->response([
            'html' => $viewHtml
        ]);
    }

    public function edit(Unit $unit)
    {
        $viewHtml = view('simple_page_view::raw.edit')->with([
            'unit' => $unit,
            'title' => 'Unit Edit Form',
            'heads' => Unit::unitHeads(),
        ])->render();

        return $this->success()->response([
            'html' => $viewHtml
        ]);
    }

    public function store(UnitStoreRequest $request)
    {
        $unit = Unit::create([
            'name' => $request->name,
            'code' => $request->code,
            'head' => $request->head,
            'status' => boolval($request->status),
        ]);

        return $this->listView('Data created successfully');
    }

    public function update(UnitUpdateRequest $request, Unit $unit)
    {
        if (Unit::where('code', $request->code)->where('id', '!=', $unit->id)->exists()) {
            return $this->message('Unit code is already in use')->response([], Response::HTTP_BAD_REQUEST);
        }

        $unit->update([
            'name' => $request->name,
            'code' => $request->code,
            'head' => $request->head,
            'status' => boolval($request->status),
        ]);

        return $this->listView('Data updated successfully');
    }

    protected function listView(string $message = '')
    {

        $units = Unit::modifyValue(
            Unit::orderBy('name', 'asc')->get()
        );

        $viewHtml = view('simple_page_view::raw.list')->with(['units' => $units])->render();

        return $this->success()->message($message)->response([
            'html' => $viewHtml
        ]);
    }

    public function remove(Unit $unit)
    {
        if (isset($unit->id) && $unit->id > 0) {
            $unit->delete();

            return $this->success()->message('Unit deleted successfully')->response();
        }

        return $this->message('Unit not found')->response();
    }
}
