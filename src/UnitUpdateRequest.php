<?php

namespace Vendor\siam;

use App\Core\Traits\ResponseJSON;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class UnitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string',
            'head' => 'required|integer',
            'name' => 'required|string',
        ];
    }

    use ResponseJSON;

    /**
     * Change form request failure response
     *
     * @param Validator $validator
     * @return HttpResponseException 
     */
    protected function failedValidation(Validator $validator)
    {

        $response = $this->response([
            'message' => $validator->errors()->first(),
        ], Response::HTTP_BAD_REQUEST);

        throw new HttpResponseException($response);
    }
}
